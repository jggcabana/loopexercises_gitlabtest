﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoopExercises
{
    class Program
    {
        static void Main(string[] args)
        {
            DrawRectangle();
        }

        private static int ExitProgram()
        {
            return 0;
        }

        private static void ClearScreen()
        {
            
        }

        private static void Continue()
        {
            
        }

        private static void DisplayMainMenu()
        {
            
        }

        private static void DisplayShapeMenu()
        {
            
        }

        private static void DisplayNumberMenu()
        {
            
        }

        private static void DrawSquare()
        {
            // Write Shape Methods here.

            int length = Convert.ToInt32(Console.ReadLine());
            for (int i = 0; i < length; i++)
            {
                for (int j = 0; j < length; j++)
                {
                    Console.Write("* ");
                }
                Console.WriteLine();
            }
        }

        private static void DrawRectangle()
        {
            // Write Shape Methods here.
            int length = Convert.ToInt32(Console.ReadLine());
            int width = Convert.ToInt32(Console.ReadLine());

            for (int i = 0; i < width; i++)
            {
                for (int j = 0; j < length; j++)
                {
                    Console.Write("* ");
                }
                Console.WriteLine();
            }
        }

        private static void DrawPyramid()
        {
            // Write Shape Methods here.
            int height = Convert.ToInt32(Console.ReadLine());
            for (int i = 0; i < height; i++)
            {
                for (int space = 1; space < height-i; space++)
                {
                    Console.Write(" ");
                }
                for (int asterisk = height - i; asterisk <= height; asterisk++)
                {
                    Console.Write("* ");
                }
                Console.WriteLine();
            }
        }

        private static void DrawDiamond()
        {
            int height = Convert.ToInt32(Console.ReadLine());
            for (int i = 0; i < height; i++)
            {
                for (int space = 1; space < height - i; space++)
                {
                    Console.Write(" ");
                }
                for (int asterisk = height - i; asterisk <= height; asterisk++)
                {
                    Console.Write("* ");
                }
                Console.WriteLine();
            }

            for (int i = 0; i < height - 1; i++)
            {
                for (int space = 0; space <= i; space++)
                {
                    Console.Write(" ");
                }
                for (int asterisk = i; asterisk < height -1 ; asterisk++)
                {
                    Console.Write("* ");
                }
                Console.WriteLine();
            }
        }

        private static void DrawTriangleLeftBase()
        {
            // Write Shape Methods here.
            int height = Convert.ToInt32(Console.ReadLine());

            for (int i = 0; i < height; i++)
            {
                for (int j = 0; j <= i; j++)
                {
                    Console.Write("* ");
                }
                Console.WriteLine();
            }
        }

        private static void DrawTrianglePascal()
        {
            // Write Shape Methods here.
        }

        private static void WriteOddNumbers()
        {
            // Write implementation for numbers here.

            ClearScreen();
            int maxNumber = Convert.ToInt32(Console.ReadLine());
            for (int i = 1; i <= maxNumber; i+=2)
            {
                Console.Write(i + " ");
            }
        }

        private static void WriteEvenNumbers()
        {
            // Write implementation for numbers here.

            ClearScreen();
            int maxNumber = Convert.ToInt32(Console.ReadLine());
            for (int i = 0; i <= maxNumber; i += 2)
            {
                Console.Write(i + " ");
            }
        }

        private static void WriteFibonacciNumbers()
        {
            // Write implementation for numbers here.

            ClearScreen();
            int first = 0, second = 1, sum = 0;
            int maxNumber = Convert.ToInt32(Console.ReadLine());

            Console.Write(first + " ");
            Console.Write(second + " ");
            for (int i = 0; first+second <= maxNumber; i++)
            {
                sum = first + second;
                Console.Write(sum + " ");
                first = second;
                second = sum;
            }
        }

        private static void WriteSkippingNumbers()
        {
            // Write implementation for numbers here

            int maxNumber = Convert.ToInt32(Console.ReadLine());
            int sequenceRatio = Convert.ToInt32(Console.ReadLine());
            for (int i = 0; i <= maxNumber; i+=sequenceRatio)
            {
                Console.Write(i+" ");
            }
        }

        private static void MovingAsterisk()
        {

        }
    }
}
